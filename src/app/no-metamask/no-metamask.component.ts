import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-metamask',
  templateUrl: './no-metamask.component.html',
  styleUrls: ['./no-metamask.component.scss']
})
export class NoMetamaskComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  reload() {
    window.location.reload();
  }

}
