import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ImageMarker } from '../services/map-helper.service';
import { UxService } from '../services/ux.service';
import { ContractService, GeoNFT } from '../services/contract.service';
import { MapHelperService } from '../services/map-helper.service';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './all-nfts.component.html',
  styleUrls: ['./all-nfts.component.scss'],
})
export class AllNFTsComponent implements OnInit, OnDestroy {
  isMobile: boolean;
  coords;
  public markers: ImageMarker[];
  public NFTs: GeoNFT[];
  subscriptions: Subscription[] = []
  constructor(
    public uxService: UxService,
    private mapHelperService: MapHelperService,
    private router: Router,
    private media: MediaMatcher,
    private contractService: ContractService,
  ) {
  }

  ngOnInit(): void {
    this.contractService.nfts$.subscribe((nfts) => {
      if (!nfts) {
        return;
      }
      this.NFTs = nfts;
      this.mapHelperService.setMultipleMarkers(this.NFTs);
    }, (err) => {
    });
    this.isMobile = this.media.matchMedia('(max-width: 700px)').matches;
    this.uxService.enableSidenav();
    this.uxService.enableLeftSidenav();
    this.subscriptions.push(
      this.contractService.getNftsToRetrieve$()
      .subscribe(async (nftsToRetrieve: GeoNFT[]) => {
        if (nftsToRetrieve.length > 0) {
          this.router.navigate(['/', 'retrieve-nfts']);
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }
}
